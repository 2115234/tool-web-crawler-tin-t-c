
CREATE DATABASE NewsCrawler;
GO

USE NewsCrawler;
GO

CREATE TABLE Results (
    Id INT PRIMARY KEY IDENTITY,
    Title NVARCHAR(255),
    Url NVARCHAR(255),
    Meta NVARCHAR(MAX),
    Description NVARCHAR(MAX),
    Author NVARCHAR(255),
    PublishDate NVARCHAR(100)
);
