﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCrawler5_News2
{
    public interface IMyObserver
    {
        void Update(List<Result> results);
    }
}
