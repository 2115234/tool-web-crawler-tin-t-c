﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace TestCrawler5_News2
{
    public class SerpScraper
    {
        private static readonly SemaphoreSlim _semaphore = new SemaphoreSlim(10); // Giới hạn số luồng chạy đồng thời

        public async Task<List<SerpResult>> ScrapeSerp(string query, int nPages)
        {
            var serpResults = new List<SerpResult>();

            for (int i = 0; i < nPages; i++)
            {
                string url = "https://www.google.com/search?q=" + WebUtility.UrlEncode(query) + "&tbm=nws&start=" + (i * 10);
                //var htmlDoc = await LoadHtmlDocument(url);
                var htmlDoc = await LoadHtmlDocument(url);
                HtmlNodeCollection nodes = htmlDoc.DocumentNode.SelectNodes("//div[@class='SoaBEf']");

                if (nodes != null)
                {
                    var results = nodes.Select(node => new SerpResult
                    {
                        Url = node.Descendants("a").FirstOrDefault()?.Attributes["href"].Value,
                        Title = WebUtility.HtmlDecode(node.SelectSingleNode(".//div[@class='n0jPhd ynAwRc MBeuO nDgy9d']")?.InnerText ?? "")
                }).ToList();


                    Parallel.ForEach(results, async (result) =>
                    {
                        await _semaphore.WaitAsync();
                        var extractedResult = await HtmlExtractor.ExtractAdditionalInfoAsync(result);
                        serpResults.Add(extractedResult);
                        _semaphore.Release();
                    });
                }
                else
                {
                    MessageBox.Show("Không thể tìm thấy kết quả trên trang " + url);
                }
            }

            return serpResults;
        }

        private async Task<HtmlAgilityPack.HtmlDocument> LoadHtmlDocument(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";

            try
            {
                var response = (HttpWebResponse)await request.GetResponseAsync();

                // Xác định mã hóa từ header Content-Type
                var encoding = GetEncodingFromContentType(response.ContentType);

                // Đọc stream phản hồi và giải mã nội dung HTML
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream, encoding))
                    {
                        var htmlContent = await reader.ReadToEndAsync();

                        var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                        htmlDoc.LoadHtml(htmlContent);

                        return htmlDoc;
                    }
                }
            }
            catch (WebException ex)
            {
                // Xử lý các ngoại lệ web ở đây (ví dụ: timeout, lỗi kết nối)
                Console.WriteLine($"WebException khi tải HTML từ {url}: {ex.Message}");
                throw;
            }
        }

        private Encoding GetEncodingFromContentType(string contentType)
        {
            Encoding encoding = Encoding.UTF8; // Mặc định là UTF-8

            if (!string.IsNullOrEmpty(contentType))
            {
                var match = Regex.Match(contentType, @"charset=([\w-]+)", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    try
                    {
                        encoding = Encoding.GetEncoding(match.Groups[1].Value);
                    }
                    catch (ArgumentException)
                    {
                        // Xử lý nếu giá trị mã hóa từ header Content-Type không hợp lệ
                        Console.WriteLine($"Mã hóa không hợp lệ được chỉ định trong header Content-Type: {match.Groups[1].Value}");
                    }
                }
            }

            return encoding;
        }


    }
}
