﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace TestCrawler5_News2
{
    public interface IHtmlExtractorStrategy
    {
        Task ExtractInfoAsync(HtmlAgilityPack.HtmlDocument htmlDoc, SerpResult result);
    }

    // Các lớp triển khai IHtmlExtractorStrategy cho từng loại trang web
    public class NhandanExtractor : IHtmlExtractorStrategy
    {
        public async Task ExtractInfoAsync(HtmlAgilityPack.HtmlDocument htmlDoc, SerpResult result)
        {
            result.Meta = GetMetaContent(htmlDoc, "description");
            result.des = htmlDoc.DocumentNode.SelectSingleNode("//div[@class='article__sapo cms-desc']")?.InnerText.Trim();
            result.Author = htmlDoc.DocumentNode.SelectSingleNode("//div[contains(@class, 'info-author')]/p[@class='name']")?.InnerText;
            HtmlNode timeNode = htmlDoc.DocumentNode.SelectSingleNode("//time[@class='time']");
            result.PublishDate = timeNode != null ? timeNode.GetAttributeValue("datetime", "") : string.Empty;
        }

        private string GetMetaContent(HtmlAgilityPack.HtmlDocument doc, string metaName)
        {
            return doc.DocumentNode.SelectSingleNode($"//meta[@name='{metaName}']")?.GetAttributeValue("content", "") ?? string.Empty;
        }
    }

    public class VnExpressExtractor : IHtmlExtractorStrategy
    {
        public async Task ExtractInfoAsync(HtmlAgilityPack.HtmlDocument htmlDoc, SerpResult result)
        {
            result.Meta = GetMetaContent(htmlDoc, "description");
            result.des = htmlDoc.DocumentNode.SelectSingleNode("//p[@class='description']")?.InnerText.Trim();
            result.Author = htmlDoc.DocumentNode.SelectSingleNode("//p[@class='Normal' and contains(@style, 'text-align:right;')]/strong")?.InnerText;
            HtmlNode timeNode = htmlDoc.DocumentNode.SelectSingleNode("//span[@class='date']");
            result.PublishDate = timeNode != null ? timeNode.InnerText.Trim() : string.Empty;
        }

        private string GetMetaContent(HtmlAgilityPack.HtmlDocument doc, string metaName)
        {
            return doc.DocumentNode.SelectSingleNode($"//meta[@name='{metaName}']")?.GetAttributeValue("content", "") ?? string.Empty;
        }
    }

    // Class HtmlExtractor sử dụng Strategy
    public static class HtmlExtractor
    {
        private static readonly SemaphoreSlim _semaphore = new SemaphoreSlim(10);

        // Dictionary ánh xạ từng loại URL vào đối tượng IHtmlExtractorStrategy tương ứng
        private static readonly Dictionary<string, IHtmlExtractorStrategy> _strategies = new Dictionary<string, IHtmlExtractorStrategy>
        {
            { "nhandan.vn", new NhandanExtractor() },
            { "vnexpress.net", new VnExpressExtractor() },
            // Thêm các loại trang web khác và các extractor tương ứng
            
        };

        public static async Task<SerpResult> ExtractAdditionalInfoAsync(SerpResult result)
        {
            try
            {
                await _semaphore.WaitAsync();

                var htmlDoc = await LoadHtmlDocument(result.Url);

                // Tìm strategy dựa trên domain của URL
                var strategy = FindStrategy(result.Url);

                // Gọi phương thức ExtractInfoAsync của strategy tương ứng
                await strategy.ExtractInfoAsync(htmlDoc, result);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi trích xuất thông tin từ " + result.Url + ": " + ex.Message);
            }
            finally
            {
                _semaphore.Release();
            }

            return result;
        }

        private static async Task<HtmlAgilityPack.HtmlDocument> LoadHtmlDocument(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";

            try
            {
                var response = (HttpWebResponse)await request.GetResponseAsync();

                // Xác định mã hóa từ header Content-Type
                var encoding = GetEncodingFromContentType(response.ContentType);

                // Đọc stream phản hồi và giải mã nội dung HTML
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream, encoding))
                    {
                        var htmlContent = await reader.ReadToEndAsync();

                        var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                        htmlDoc.LoadHtml(htmlContent);

                        return htmlDoc;
                    }
                }
            }
            catch (WebException ex)
            {
                // Xử lý các ngoại lệ web ở đây (ví dụ: timeout, lỗi kết nối)
                Console.WriteLine($"WebException khi tải HTML từ {url}: {ex.Message}");
                throw;
            }
        }

        private static Encoding GetEncodingFromContentType(string contentType)
        {
            Encoding encoding = Encoding.UTF8; // Mặc định là UTF-8

            if (!string.IsNullOrEmpty(contentType))
            {
                var match = Regex.Match(contentType, @"charset=([\w-]+)", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    try
                    {
                        encoding = Encoding.GetEncoding(match.Groups[1].Value);
                    }
                    catch (ArgumentException)
                    {
                        // Xử lý nếu giá trị mã hóa từ header Content-Type không hợp lệ
                        Console.WriteLine($"Mã hóa không hợp lệ được chỉ định trong header Content-Type: {match.Groups[1].Value}");
                    }
                }
            }

            return encoding;
        }

        private static IHtmlExtractorStrategy FindStrategy(string url)
        {
            // Tìm strategy dựa vào domain của URL
            foreach (var strategy in _strategies)
            {
                if (url.Contains(strategy.Key))
                {
                    return strategy.Value;
                }
            }

            // Nếu không tìm thấy strategy nào thích hợp, sử dụng strategy mặc định hoặc xử lý tùy ý
            return new DefaultExtractor();
        }
    }

    // Class mặc định cho trường hợp không có strategy nào phù hợp
    public class DefaultExtractor : IHtmlExtractorStrategy
    {
        public async Task ExtractInfoAsync(HtmlAgilityPack.HtmlDocument htmlDoc, SerpResult result)
        {
            result.Meta = GetMetaContent(htmlDoc, "description");
            result.des = htmlDoc.DocumentNode.SelectSingleNode("//meta[@property='og:description']")?.GetAttributeValue("content", "");
            result.Author = GetMetaContent(htmlDoc, "author");
            HtmlNode timeNode = htmlDoc.DocumentNode.SelectSingleNode("//meta[@property='article:published_time']");
            result.PublishDate = timeNode != null ? timeNode.GetAttributeValue("content", "") : string.Empty;
        }

        private string GetMetaContent(HtmlAgilityPack.HtmlDocument doc, string metaName)
        {
            return doc.DocumentNode.SelectSingleNode($"//meta[@name='{metaName}']")?.GetAttributeValue("content", "") ?? string.Empty;
        }
    }
}
