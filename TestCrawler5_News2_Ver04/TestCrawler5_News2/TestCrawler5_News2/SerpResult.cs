﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCrawler5_News2
{
    public class SerpResult
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Meta { get; set; }
        public string des { get; set; }
        public string Author { get; set; }
        public string PublishDate { get; set; }
    }

    public class Result
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Meta { get; set; }
        public string des { get; set; }
        public string Author { get; set; }
        public string PublishDate { get; set; }
    }
    public class SerpResultAdapter : IResultAdapter
    {
        private readonly SerpResult _serpResult;

        public SerpResultAdapter(SerpResult serpResult)
        {
            _serpResult = serpResult;
        }

        public Result Convert()
        {
            return new Result
            {
                Title = _serpResult.Title,
                Url = _serpResult.Url,
                Meta = _serpResult.Meta,
                des = _serpResult.des,
                Author = _serpResult.Author,
                PublishDate = _serpResult.PublishDate
            };
        }
    }

    public static class SerpResultExtensions
    {
        public static Result ToResult(this SerpResult serpResult)
        {
            var adapter = new SerpResultAdapter(serpResult);
            return adapter.Convert();
        }
    }

}
