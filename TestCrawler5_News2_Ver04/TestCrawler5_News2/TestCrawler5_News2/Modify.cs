﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TestCrawler5_News2.Modify;

namespace TestCrawler5_News2
{
    internal class Modify: ITaiKhoanFactory
    {
        public Modify()
        {
        }
        public interface ITaiKhoanFactory
        {
            TaiKhoan CreateTaiKhoan(string tenTaiKhoan, string matKhau);
        }
        public TaiKhoan CreateTaiKhoan(string tenTaiKhoan, string matKhau)
        {
            return new TaiKhoan(tenTaiKhoan, matKhau);
        }

        public List<TaiKhoan> TaiKhoans(string query)
        {
            List<TaiKhoan> taiKhoans = new List<TaiKhoan>();
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = new SqlCommand(query, sqlConnection))
                using (SqlDataReader dataReader = sqlCommand.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        taiKhoans.Add(CreateTaiKhoan(dataReader.GetString(0), dataReader.GetString(1)));
                    }
                }

                sqlConnection.Close();
            }
            return taiKhoans;
        }

        public void Command(string query)
        {
            using (SqlConnection sqlConnection = Connection.GetSqlConnection())
            {
                sqlConnection.Open();
                using (SqlCommand sqlCommand = new SqlCommand(query, sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }
                sqlConnection.Close();
            }
        }
    }
}