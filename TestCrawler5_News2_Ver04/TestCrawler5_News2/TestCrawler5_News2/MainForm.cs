﻿using AE.Net.Mail;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static TestCrawler5_News2.MainForm;


namespace TestCrawler5_News2
{
    public partial class MainForm : Form, IMyObserver
    {
        private DatabaseHelper _databaseHelper;
       
        
        public MainForm()
        {
            InitializeComponent();
            _databaseHelper = DatabaseHelper.Instance; // Sử dụng instance của DatabaseHelper

            // Đăng ký MainForm làm Observer
            _databaseHelper.RegisterObserver(this);
        }

        public void Update(List<Result> results)
        {
            // Invoke để đảm bảo an toàn đối với luồng chính
            if (dgvResults.InvokeRequired)
            {
                dgvResults.Invoke(new Action(() => AddDataToDataGridView(results)));
            }
            else
            {
                AddDataToDataGridView(results);
            }
        }
        private async void btnSearch_Click(object sender, EventArgs e)
        {
            string keyword = txtKeyword.Text;
            var scraper = new SerpScraper();
            var serpResults = await scraper.ScrapeSerp(keyword, 5);

            var tasks = serpResults.Select(result => Task.Run(async () =>
            {
                await DatabaseHelper.Instance.SaveResultAsync(result.ToResult()); // Chuyển đổi SerpResult sang Result ở đây
            })).ToArray();

            await Task.WhenAll(tasks);

            var results = serpResults.Select(result => result.ToResult()).ToList();

            if (results.Count > 0)
            {
                AddDataToDataGridView(results);
            }
            else
            {
                MessageBox.Show($"Không tìm thấy kết quả cho từ khóa '{keyword}'.");
            }

            MessageBox.Show("Dữ liệu đã được lưu vào cơ sở dữ liệu.");
        }


        private async void btnLoadData_Click(object sender, EventArgs e)
        {
            var results = await DatabaseHelper.Instance.LoadResultsAsync();

            if (results.Count > 0)
            {
                if (dgvResults.InvokeRequired)
                {
                    dgvResults.Invoke(new Action(() => AddDataToDataGridView(results)));
                }
                else
                {
                    AddDataToDataGridView(results);
                }
            }
            else
            {
                MessageBox.Show("Không có dữ liệu để hiển thị.");
            }
        }

        private void AddDataToDataGridView(List<Result> results)
        {
            dgvResults.Rows.Clear(); // Xóa dữ liệu cũ trước khi thêm mới

            foreach (var result in results)
            {
                dgvResults.Rows.Add(result.Title, result.Url, result.Meta, result.des, result.Author, result.PublishDate);

            }

            // Cài đặt lại font và các thuộc tính của DataGridView
            dgvResults.DefaultCellStyle.Font = new Font("Arial", 8);
            foreach (DataGridViewColumn column in dgvResults.Columns)
            {
                column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
        }

        private void DgvResults_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvResults.ResumeLayout();
        }

        private async void btnExportToCsv_Click(object sender, EventArgs e)
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv";
            saveFileDialog.Title = "Export to CSV";
            saveFileDialog.ShowDialog();

            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                await DatabaseHelper.ExportResultsToCsvAsync(saveFileDialog.FileName);
            }
        }

        private async void btnReset_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Bạn có chắc chắn muốn xóa hết dữ liệu từ cơ sở dữ liệu?", "Xác nhận xóa dữ liệu", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                await DatabaseHelper.Instance.ResetDatabaseAsync();
                dgvResults.Rows.Clear(); // Xóa dữ liệu trong DataGridView nếu cần
            }
        }
    }
}
