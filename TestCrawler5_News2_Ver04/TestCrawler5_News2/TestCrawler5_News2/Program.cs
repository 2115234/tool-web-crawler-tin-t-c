﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TestCrawler5_News2
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Formlogin());
            //Application.Run(new MainForm());
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Formlogin formLogin = new Formlogin();
            if (formLogin.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new MainForm());
            }
        }
    }
}
