﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static TestCrawler5_News2.MainForm;

namespace TestCrawler5_News2
{
    public class DatabaseHelper
    {
        private static readonly Lazy<DatabaseHelper> instance = new Lazy<DatabaseHelper>(() => new DatabaseHelper());
        public static DatabaseHelper Instance => instance.Value;

        private readonly string _connectionString;

        private DatabaseHelper()
        {
            // Khởi tạo kết nối đến cơ sở dữ liệu từ file cấu hình (App.config hoặc Web.config)
            _connectionString = ConfigurationManager.ConnectionStrings["NewsCrawlerDB"].ConnectionString;
        }

        private async Task<SqlConnection> GetConnectionAsync()
        {
            var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
            return connection;
        }

        public async Task<bool> TestConnectionAsync()
        {
            try
            {
                using (var connection = await GetConnectionAsync())
                {
                    return connection.State == ConnectionState.Open;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kết nối cơ sở dữ liệu thất bại: " + ex.Message);
                return false;
            }
        }

        private List<IMyObserver> _observers = new List<IMyObserver>();

        // Đăng ký Observer
        public void RegisterObserver(IMyObserver observer)
        {
            _observers.Add(observer);
        }

        // Hủy đăng ký Observer
        public void UnregisterObserver(IMyObserver observer)
        {
            _observers.Remove(observer);
        }

        // Phương thức để thông báo cho các Observer khi có sự thay đổi
        private void NotifyObservers(List<Result> results)
        {
            foreach (var observer in _observers)
            {
                observer.Update(results);
            }
        }

        // Phương thức load dữ liệu từ cơ sở dữ liệu và thông báo cho các Observer
        public async Task LoadResultsAsync(string keyword, int count)
        {
            var scraper = new SerpScraper();
            var serpResults = await scraper.ScrapeSerp(keyword, count);

            var tasks = serpResults.Select(result => Task.Run(async () =>
            {
                await SaveResultAsync(result.ToResult());
            })).ToArray();

            await Task.WhenAll(tasks);

            var results = serpResults.Select(result => result.ToResult()).ToList();
            NotifyObservers(results);
        }

            public async Task SaveResultAsync(Result result)
        {
            using (var connection = await GetConnectionAsync())
            {
                using (var command = new SqlCommand("INSERT INTO Results (Title, Url, Meta, Description, Author, PublishDate) VALUES (@Title, @Url, @Meta, @Description, @Author, @PublishDate)", connection))
                {
                    command.Parameters.AddWithValue("@Title", result.Title);
                    command.Parameters.AddWithValue("@Url", result.Url);
                    command.Parameters.AddWithValue("@Meta", result.Meta ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Description", result.des ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@Author", result.Author ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@PublishDate", result.PublishDate ?? (object)DBNull.Value);

                    await command.ExecuteNonQueryAsync();
                }
            }
        }

        public async Task<List<Result>> LoadResultsAsync()
        {
            var results = new List<Result>();

            using (var connection = await GetConnectionAsync())
            {
                using (var command = new SqlCommand("SELECT Title, Url, Meta, Description, Author, PublishDate FROM Results", connection))
                {
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            results.Add(new Result
                            {
                                Title = reader["Title"].ToString(),
                                Url = reader["Url"].ToString(),
                                Meta = reader["Meta"]?.ToString(),
                                des = reader["Description"]?.ToString(),
                                Author = reader["Author"]?.ToString(),
                                PublishDate = reader["PublishDate"]?.ToString()
                            });
                        }
                    }
                }
            }

            return results;
        }

        public async Task ResetDatabaseAsync()
        {
            try
            {
                using (var connection = await GetConnectionAsync())
                {
                    using (var command = new SqlCommand("DELETE FROM Results", connection))
                    {
                        await command.ExecuteNonQueryAsync();
                    }
                }
                MessageBox.Show("Đã xóa dữ liệu thành công từ cơ sở dữ liệu.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi xóa dữ liệu từ cơ sở dữ liệu: " + ex.Message);
            }
        }


        public static async Task ExportResultsToCsvAsync(string filePath)
        {
            try
            {
                var results = await DatabaseHelper.Instance.LoadResultsAsync();

                using (var writer = new StreamWriter(filePath))
                {
                    await writer.WriteLineAsync("Title,Url,Meta,Description,Author,PublishDate");

                    foreach (var result in results)
                    {
                        await writer.WriteLineAsync($"{EscapeCsvField(result.Title)},{EscapeCsvField(result.Url)},{EscapeCsvField(result.Meta)},{EscapeCsvField(result.des)},{EscapeCsvField(result.Author)},{EscapeCsvField(result.PublishDate)}");
                    }
                }

                MessageBox.Show($"Xuất dữ liệu thành công vào tệp {filePath}", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi xuất dữ liệu ra file CSV: " + ex.Message);
            }
        }

        private static string EscapeCsvField(string field)
        {
            if (string.IsNullOrEmpty(field))
                return string.Empty;

            // Nếu có dấu phẩy trong trường, đặt trường trong dấu ngoặc kép để tránh lỗi phân tách
            if (field.Contains(","))
                return $"\"{field.Replace("\"", "\"\"")}\"";
            else
                return field;
        }

    }


}
